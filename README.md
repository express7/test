# test

[![npm version](https://img.shields.io/npm/v/express-validator.svg)](https://git.immobisp.com/express/test)
[![Build Status](https://img.shields.io/travis/express-validator/express-validator.svg)](https://git.immobisp.com/express/test)
[![Coverage Status](https://img.shields.io/coveralls/express-validator/express-validator.svg)](https://git.immobisp.com/express/test/-/tree/main)

An [express.js](https://github.com/visionmedia/express) test with
[Mocha test](https://git.immobisp.com/express/test).

- [Installation](#installation)
- [Documentation](#documentation)
- [Changelog](#changelog)
- [License](#license)

## Installation

```console
npm install git+https://git.immobisp.com/express/test.git
```

You may then access it as follows:

```console
npm run test
```

Also make sure that you have Node.js 8 or newer in order to use it.

## Documentation

Please refer to the documentation website on https://git.immobisp.com/express/test/-/blob/master/README.md.

## Changelog

Check the [GitLab Releases page](https://git.immobisp.com/express/test/-/blob/master/CHANGELOG.md).

## License

MIT License
