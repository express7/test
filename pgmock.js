'use strict'

const debug = require('debug')('knex:client:pgmock')
const _ = require('lodash')
const Client = require('knex/lib/dialects/postgres')

class Client_PGmock extends Client {
  _driver () {
    return require('pgmock2')
  }

  database() {
    if (!this._database) {
      const { default: pgmock, getPool } = this.driver
      this._database = new pgmock()
    }
  
    return this._database
  }
  
  acquireRawConnection() {
    return this.database().connect()
  }

  _query(connection, obj) {
    if (!obj.sql) throw new Error('The query is empty')

    return connection.query(obj.sql, obj.bindings)
  }

  processResponse(obj, runner) {
    const { rows = obj } = obj

    return rows
  }
}
 
Object.assign(Client_PGmock.prototype, {
  // The "dialect", for reference .
  driverName: 'pgmock'
})

module.exports = Client_PGmock
