'use strict'

const _ = require('app/helpers')

return _.dirRequire(__dirname, { include: /\.spec\.js$/ })
