'use strict'

const Models = require('app/model/models')
const { assert } = require('test')

module.exports = describe('Model', async () => {
  for (const [name, Model] of Object.entries(Models)) {
    const [pkey] = Model.keys

    describe(name, async () => {
      // create
      it(`create ${name}`, async () => {
        const model = new Model({ [pkey]: '?' })

        assert.equal(model[pkey], '?')
      })

      // save
      it(`save ${name}`, async () => {
        const model = new Model({ [pkey]: 1 })
        await model.save()

        assert.equal(model[pkey], 1)
      })

      // count
      it(`count ${name}`, async () => {
        const count = await Model.count()

        assert.equal(count, 1)
      })

      // find
      it(`find ${name}`, async () => {
        const [model] = await Model.find()

        assert.equal(model[pkey], 1)
      })

      // remove
      it(`remove ${name}`, async () => {
        const model = await (new Model({ [pkey]: 1 })).remove()

        assert.equal(model[pkey], 1)
      })
    })
  }
})
