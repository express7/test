'use strict'

const faker = require('faker')
const leven = require('leven')
const _ = require('app/helpers')

const fakers = []
for (const namespace in faker) {
  for (const name in faker[namespace]) {
    fakers.push({ name, namespace, fn: faker[namespace][name] })
  }
}

// guest object properties
const guess = (name, level = 3) => {
  const [result] = fakers.filter(f => {
    const wordA = _.lowerCase(name).replace(/ /g, '')
    const wordB = _.lowerCase(f.name).replace(/ /g, '')
    if (wordA.indexOf(wordB) >= 0 || wordB.indexOf(wordA) >= 0) return true

    return leven(name, f.name) < level
  }).sort(f => leven(f.name, name))

  if (result && result.fn && typeof result.fn === 'function') return result.fn
}

// generate objects properties
module.exports = (Model, count, options = {}) => {
  const { props = Model, keys: { 0: ID } } = Model

  const fake = Object.keys(props)
    .filter(key => props[key] !== 'increments')
    .reduce((a, c) => ({ ...{ [c]: guess(c) }, ...a }), {})

  const mocker = { ...fake, ...options }

  const samples = Array.from(Array(count + 1).keys()).map(id => Object.keys(mocker)
    .map((key) => ({ [key]: typeof mocker[key] === 'function' ? mocker[key]() : mocker[key] }))
    .reduce((a, c) => ({ ...c, ...a }), { [ID]: id + 1 })
  )

  const sample = samples.pop()

  Model.samples = samples
  Model.sample = sample

  return samples
}

