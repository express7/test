'use strict'

const { app, req, paths } = require('test')
const queries = { get: 'query', post: 'send' }

module.exports = describe('Regression', async () => {
  for (const key in paths) {
    describe(key, async function () {
      // this.timeout(5000)

      paths[key].forEach(({ method, url }) => {
        if (queries[method]) {
          // unauthorized
          it(`${method} ${url} [unauthorized]`, async () => {
            let res = req(app)[method](url)

            res = await res[queries[method]]()

            .expect(401)
          })

          it(`${method} ${url}`, async () => {
            let res = req(app)[method](url)
              .auth(token, { type: 'bearer' })

            res = await res[queries[method]]()

            .expect(200)
          })
        }
      })
    })
  }
})
