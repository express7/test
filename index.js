'use strict'

process.env.NODE_ENV = 'test'

const request = require('supertest')
const assert = require('assert')
const _ = require('app/helpers')
const hash = require('object-hash')
const Database = require('app/database')

const getKey = sql => hash(_.words(sql), { algorithm: 'md5' })
const unInterceptQueries = {}
const interceptQueries = {
  add (...args) {
    const [sql, ...rest] = arguments
    const [response] = rest.reverse()
    interceptQueries[getKey(sql)] = { sql, response }
  }
}

Object.assign(Database.context.client, { interceptQueries, unInterceptQueries })

const _query = Database.context.client.query
Database.context.client.query = (connection, queryParam) => {
  const key = getKey(queryParam.sql)

  const { response } = interceptQueries[key] || {}

  if (response) return new Promise(r => r({ response }))

  unInterceptQueries[key] = queryParam.sql

  return response
    ? new Promise(r => r({ response }))
    : _query.apply(Database.context.client, [connection, queryParam])
}

// modify hash
require('app/auth/password').hash = pass => pass
require('app/auth/password').compare = (a, b) => a === b

const app = require('app')
const { client } = require('app/database')
const { getRoutes } = require('app/router')
const { config } = require('app/auth')
const { secretOrKey, statusField, activeState } = config
const generate = require('./faker')
const Models = require('./model')

const paths = ((options, router) => {
  const routes = Object.entries(getRoutes(router)).reduce((a, [path, methods]) => {
    if (path === '/swagger.json') return a

    if (path.indexOf(':model') > -1) return a

    const url = path.replace(/\.[^/.]+$/gi, '') // remove extension
      .replace(/\?/gi, '') // remove ?

    for (const [method, v] of Object.entries(methods)) {
      const group = v.rpath.split('/').pop()
      a[group] = !a[group] ? [{ url, method }] : [...a[group], { url, method }]
    }

    return a
  }, {})
  
  // delete auth
  delete routes.auth

  return routes
})({}, app)

module.exports = {
  app,
  paths,
  // ...chai,
  // req: chai.request,
  // should: chai.should(),
  request,
  req: request,
  assert,
  generate,
  createToken: () => require('app/auth').sign({ id: 1, [statusField]: activeState, role: ['admin'] }, secretOrKey, { expiresIn: '300s' }),
  getRoutes,
  client,
  sql: interceptQueries,
  failedSql: unInterceptQueries,
  lng: 106.833301, 
  lat: -6.345551,
  zoom: 15
}
