'use strict'

const debug = require('debug')('knex:client:pg-mem')
const hash = require('object-hash')
const _ = require('app/helpers')
const Client = require('knex/lib/dialects/postgres')
const getKey = sql => hash(_.words(sql), { algorithm: 'md5' })
const unInterceptQueries = {}
const interceptQueries = {
  add (...args) {
    const sql = arguments[0]
    const res = arguments[arguments.length - 1]

    interceptQueries[getKey(sql)] = { sql, res }
  }
}

class Client_PGmem extends Client {
  constructor (config) {
    super({ queryLatency: 0, ...config, connection: {} })
  }

  _driver () {
    return require('pg-mem')
  }

  initializeDriver () {
    try {
      const client = this

      this.driver = this.database().adapters.createPg(this.config.queryLatency)

      // intercept queries
      this.interceptQueries = interceptQueries
      this.unInterceptQueries = unInterceptQueries

      this.database().public.interceptQueries(sql => {
        const key = getKey(sql)
        const query = client.interceptQueries[key]

        if (!query) client.unInterceptQueries[key] = sql

        return query ? query.res : null
      })

    } catch (e) {
      const message = `Knex: run\n$ npm install ${this.driverName} --save`;
      this.logger.error(`${message}\n${e.message}\n${e.stack}`);
      throw new Error(`${message}\n${e.message}`);
    }
  }

  database () {
    if (!this._database) this._database = this._driver().newDb()

    return this._database
  }
}
 
Object.assign(Client_PGmem.prototype, {
  // The "dialect", for reference .
  driverName: 'pgmem',
  version: 'pg-mem'
})

module.exports = Client_PGmem
