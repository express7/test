'use strict'

const _ = require('app/helpers')
const Database = require('app/database')
const sql = Database.client.interceptQueries
const Models = require('app/model/models')

// delete uneeded model
delete Models.Log

// model mock
for (const [name, Model] of Object.entries(Models)) {
  const [pkey] = Model.keys
  const [prkey] = Model.primaryKeys

  // save
  const mergeColumns = _.chain([...Object.values(Model.columns), 'updated_at']).difference([...Model.primaryKeys, 'created_at', 'createdAt']).uniq().sort().value() 
  sql.add(Database(Model.table).withSchema(Model.schema).insert({ created_at: Database.fn.now(), [prkey]: 1, updated_at: Database.fn.now() }).onConflict(Model.primaryKeys).merge(mergeColumns).returning('*').toSQL().sql, [{ [prkey]: 1 }])

  // count
  sql.add(Database(Model.table).withSchema(Model.schema).count('*', { as: 'count' }).limit(1).toSQL().sql, [{ count: 1 }])

  // find
  sql.add(Database(Model.table).withSchema(Model.schema).limit(1).toSQL().sql, [{ [pkey]: 1 }])

  // find by id
  sql.add(Database(Model.table).withSchema(Model.schema).where(prkey, 1).limit(1).toSQL().sql, [{ [pkey]: 1 }])

  // delete
  sql.add(Database(Model.table).withSchema(Model.schema).where(prkey, 1).del().returning(prkey).toSQL().sql, [{ [pkey]: 1 }])
}

module.exports = Models
