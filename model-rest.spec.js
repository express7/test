'use strict'

const _ = require('app/helpers')
const Models = require('app/model/models')
const { app, req } = require('test')

module.exports = describe('API', async () => {
  for (const [key, Model] of Object.entries(Models)) {
    const name = _.kebabCase(key)
    const [pkey] = Model.keys
    const [prkey] = Model.primaryKeys

    describe(name, () => {
      // create unauthorized
      it(`create /api/${name} [unauthorized]`, async () => {
        const res = await req(app)
          .post(`/api/${name}`)
          .send({ [pkey]: 1 })

          .expect(401)
      })

      // create
      it(`create /api/${name}`, async () => {
        const res = await req(app)
          .post(`/api/${name}`)
          .auth(token, { type: 'bearer' })
          .send({ [pkey]: 1 })

          .expect(201)
      })

      // list unauthorized
      it(`read /api/${name} [unauthorized]`, async () => {
        const res = await req(app)
          .get(`/api/${name}`)

          .expect(401)
      })

      // list
      it(`read /api/${name}`, async () => {
        const res = await req(app)
          .get(`/api/${name}`)
          .auth(token, { type: 'bearer' })

          .expect(200)
      })

      // get by id unauthorized
      it(`readBy /api/${name}/:id [unauthorized]`, async () => {
        const res = await req(app)
          .get(`/api/${name}/1`)

          .expect(401)
      })

      // get by id
      it(`readBy /api/${name}/:id`, async () => {
        const res = await req(app)
          .get(`/api/${name}/1`)
          .auth(token, { type: 'bearer' })

          .expect(200)
      })

      // update unauthorized
      it(`update /api/${name}/:id [unauthorized]`, async () => {
        const res = await req(app)
          .put(`/api/${name}/1`)
          .send({ [pkey]: 1 })

          .expect(401)
      })

      // update
      it(`update /api/${name}/:id`, async () => {
        const res = await req(app)
          .put(`/api/${name}/1`)
          .auth(token, { type: 'bearer' })
          .send({ [pkey]: 1 })

          .expect(200)
      })

      // delete unauthorized
      it(`delete /api/${name}/:id [unauthorized]`, async () => {
        const res = await req(app)
          .delete(`/api/${name}/1`)

          .expect(401)
      })

      // delete
      it(`delete /api/${name}/:id`, async () => {
        const res = await req(app)
          .delete(`/api/${name}/1`)
          .auth(token, { type: 'bearer' })

          .expect(200)
      })
    })
  }
})